# The-List-Item-Click-Method
@Override
 protected void onListItemClick(ListView 1, View v, int position, long id) {
     Uri uri = ContentUris.withAppendedId(getIntent().getData(), id);
     String action = getIntent().getAction();
     If(Intent.ACTION_PICK.equals(action) ||
Intent.ACTION_GET_CONTENT.equals(action)) {
        setResult(RESULT_OK, new Intent().setData(uri));
    }else {
      startActivity(new Intent(Intent.ACTION_EDIT, uri));
    }
 }
